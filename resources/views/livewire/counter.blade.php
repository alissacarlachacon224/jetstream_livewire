
<div style="text-align: center">
    <h1 class="text-3xl font-bold underline">Hello World!</h1>
    <button wire:click="increment">+</button>
    <h1>{{ $count }}</h1>
    <button wire:click="decrement">-</button>
</div>
