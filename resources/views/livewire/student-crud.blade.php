
<div class="max-w-6xl mx-auto">
    <div class="flex justify-end m-2 p-2">
        <x-jet-button wire:click="openCreateModal">AAAA</x-jet-button>
    </div>
    <div class="m-2 p-2">
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                    <table class="table-fixed w-full">
                        <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2">First Name</th>
                                <th class="px-4 py-2">Last Name</th>
                                <th class="px-4 py-2">Course</th>
                                <th class="px-4 py-2">Email</th>
                                <th class="px-4 py-2">Mobile</th>
                                <th class="px-4 py-2">Action</th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach($students as $student)
                            <tr>
                               
                                <td class="border px-4 py-2">{{$student->first_name}}</td>
                                <td class="border px-4 py-2">{{$student->last_name}}</td>
                                <td class="border px-4 py-2">{{$student->course}}</td>
                                <td class="border px-4 py-2">{{$student->email}}</td>
                                <td class="border px-4 py-2">{{$student->mobile}}</td>
                                <td class="border px-4 py-2">
                                    <x-jet-button wire:click="showEditStudentModal({{$student->id}})">Edit</x-jet-button>
                                    <x-jet-button wire:click="deleteStudent({{$student->id}})">Delete</x-jet-button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div>
        <x-jet-dialog-modal wire:model="showCreateModal">
            @if ($isEdit)
                <x-slot name="title">Update Student</x-slot>
            @else
                <x-slot name="title">Add Student</x-slot>
            @endif
            <x-slot name="content">
                <div class="space-y-8 divide-y divide-gray-200">
                    <form enctype="mulipart/form-data">
                        <div class="sm:col-span-6">
                            <label for="first_name" class="block text-sm font-medium text-gray-700">First Name</label>
                            <div class="mt-1">
                                <input type="text" id="first_name" wire:model.lazy="first_name" name="first_name" class="block w-full appearance-none bg-white border rounded-md">
                            </div>
                        </div>
                        <div class="sm:col-span-6">
                            <label for="last_name" class="block text-sm font-medium text-gray-700 mt-2">Last Name</label>
                            <div class="mt-1">
                                <input type="text" id="last_name" wire:model.lazy="last_name" name="last_name" class="block w-full appearance-none bg-white border rounded-md">
                            </div>
                        </div>
                        <div class="sm:col-span-6">
                            <label for="email" class="block text-sm font-medium text-gray-700 mt-2">Email</label>
                            <div class="mt-1">
                                <input type="text" id="email" wire:model.lazy="email" name="email" class="block w-full appearance-none bg-white border rounded-md">
                            </div>
                        </div>
                        <div class="sm:col-span-6">
                            <label for="mobile" class="block text-sm font-medium text-gray-700 mt-2">Mobile</label>
                            <div class="mt-1">
                                <input type="text" id="mobile" wire:model.lazy="mobile" name="mobile" class="block w-full appearance-none bg-white border rounded-md">
                            </div>
                        </div>
                        <div class="sm:col-span-6">
                            <label for="mobile" class="block text-sm font-medium text-gray-700 mt-2">Course</label>
                            <div class="mt-1">
                          
                                <select id="course" wire:model.lazy="course" name="course" class="block w-full appearance-none bg-white border rounded-md">
                                    <option value="" selected>Select Course</option>
                                    <option value="Computer Science">Computer Science</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>   
            </x-slot>
            <x-slot name="footer">
                @if($isEdit)
                    <x-jet-button wire:click="update">Update</x-jet-button>
                @else
                <x-jet-button wire:click="store">Save</x-jet-button>
                @endif
            </x-slot>
        </x-jet-dialog-modal>
    </div>
</div>