<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;   
    protected $fillable = [
        'first_name',
        'last_name', 
        'email', 
        'mobile',
       // 'course_id'
       'course'
    ]; 

    /*
    
    public function course(){
        return $this->belongsTo('App\Models\Course');
    }

    */
}
