<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Student;
use App\Models\Courses;



class StudentCrud extends Component
{
    
    public $showCreateModal = false;
    public  $first_name, $last_name, $email, $mobile, $course_id, $student_id, $course;
    public $student;
    public $isEdit = false;
    //public $selectedCourse = null;
 
    public function mount()
    {
       // $this->courses = Courses::all();
    }

    public function openCreateModal()
    {

        $this->showCreateModal = true;
    }
    

    public function render()
    {
        return view('livewire.student-crud',['students' => Student::all()]);
    }

    public function store() 
    {
        
        $this->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'course_id' => 'required',
        ]);
        
    
        Student::Create([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'mobile' => $this->mobile,
           // 'course_id' => $this->course_id,
           'course' => $this->course

        ]);

        $this->reset();
    }

    public function showEditStudentModal($id){

        $this->student = Student::findOrFail($id);
        $this->first_name = $this->student ->first_name;
        $this->last_name = $this->student ->last_name;
        $this->email = $this->student ->email;
        $this->mobile = $this->student ->mobile;
        $this->course = $this->student ->course;
        $this->isEdit = true;
        $this->showCreateModal = true;
    }

    public function update(){

        $this->student->update([

            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'mobile' => $this->mobile,
           // 'course_id' => $this->course_id,
           'course' => $this->course

        ]);

        $this->reset();
    }

    public function deleteStudent($id){

        Student::findOrFail($id)->delete();
        $this->reset();

    }
    
}
